<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
    <form action="{{ route('welcome') }}" method="post">
    @csrf
        <h1>Buat Account Baru</h1>
        <h2>Sign Up Form</h2>
        <p>First name :</p>
        <input type="text" name="fName">
        <p>Last name :</p>
        <input type="text" name="lName">
        <p>Gender</p>
        <input type="radio" name="gender" value="Male"> Male <br>
        <input type="radio" name="gender" value="Female"> Female
        <p>Nationality</p>
        <select name="nationality">
            <option value="" disabled></option><br>
            <option value="Indonesia">Indonesia</option><br>
            <option value="Belanda">Belanda</option>
        </select>
        <p>Language Spoken</p>
        <input type="checkbox" name="gender" value="Bahasa Indonesia"> Bahasa Indonesia <br>
        <input type="checkbox" name="gender" value="English"> English <br>
        <input type="checkbox" name="gender" value="Other"> Other
        <p>Bio</p>
        <textarea name="bio" rows="8"></textarea><br>
        <button type="submit" name="simpan">Sign Up</button>
    </form>
</body>
</html>