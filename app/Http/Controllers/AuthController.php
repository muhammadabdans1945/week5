<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function index()
    {
        return view('form');
    }
    public function welcome(Request $req)
    {
        $data = array(
            'nama' => $req->fName.' '.$req->lName
        );
        return view('dashboard',compact('data'));
    }
}
